﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace Subida_de_Archivos_FTP
{
    class Program
    {
        static void Main(string[] args)
        {
            using(WebClient wftp = new WebClient())
            {
                string usuario      = ConfigurationManager.AppSettings.Get("usuario");
                string clave        = ConfigurationManager.AppSettings.Get("clave");
                string rutaFTP      = ConfigurationManager.AppSettings.Get("rutaFTP");
                string rutaJSON      = ConfigurationManager.AppSettings.Get("rutaJSONaCopiar");
                string rutaArchivo  = ConfigurationManager.AppSettings.Get("rutaArchivo");
                //string fileDate = File.GetLastWriteTime(rutaArchivo).ToString("dd/MM/yyyy");
                string filename = DateTime.Today.ToString("yyyy-MM-dd") + ".JSON";
                rutaArchivo += filename;
                try
                {

                    File.Copy(rutaJSON + filename, rutaArchivo);

                    wftp.Credentials = new NetworkCredential(usuario, clave);
                    Uri uri = new Uri(rutaFTP+filename);


                    wftp.UploadFile(uri, rutaArchivo);
                    Wftp_UploadFileCompleted();

                }
                catch (Exception e)
                {
                    enviarNotificacion("Error --> " + e.ToString());
                }
                /*if (fileDate != hoy)
                {
                    string status = ConfigurationManager.AppSettings.Get("mensajeNegativo") + "Ruta --> " + rutaArchivo + " " + hoy;
                    enviarNotificacion(status);
                }
                else
                {
                    try
                    {
                        wftp.Credentials = new NetworkCredential(usuario, clave);
                        Uri uri = new Uri(rutaFTP);
                        

                        wftp.UploadFile(uri, rutaArchivo);
                        Wftp_UploadFileCompleted();

                    }
                    catch (Exception e)
                    {
                        enviarNotificacion("Error --> " + e.ToString());
                    }
                }*/

            }
        }

        private static void Wftp_UploadFileCompleted()
        {
            string rutaFTP = ConfigurationManager.AppSettings.Get("rutaFTP");
            string rutaArchivo = ConfigurationManager.AppSettings.Get("rutaArchivo");
            string status = ConfigurationManager.AppSettings.Get("mensajePositivo") + "Ruta Archivo --> " + rutaArchivo + " Cargado Satisfactoriamente en Website Advanced ruta --> " + rutaFTP; 
            enviarNotificacion(status);
            
        }

        private static void enviarNotificacion(string status)
        {
            //string rutaArchivo = ConfigurationManager.AppSettings.Get("rutaArchivo");
            string smtp = ConfigurationManager.AppSettings.Get("smtp");
            string puerto = ConfigurationManager.AppSettings.Get("puerto");
            string SSL = ConfigurationManager.AppSettings.Get("SSL");
            string usrCorreo = ConfigurationManager.AppSettings.Get("usuarioCorreo");
            string claveCorreo = ConfigurationManager.AppSettings.Get("claveCorreo");
            string correoVis = ConfigurationManager.AppSettings.Get("correoVisualizar");
            string Subject = ConfigurationManager.AppSettings.Get("Subject");


            SmtpClient client = new SmtpClient(smtp);
            client.Port = Convert.ToInt32(puerto);
            client.EnableSsl = Convert.ToBoolean(SSL);
            NetworkCredential credential = new NetworkCredential(usrCorreo, claveCorreo);
            MailMessage msg = new MailMessage();
            MailAddress fromMailAddress = new MailAddress(correoVis);


            //Attachment attachment = new Attachment(rutaArchivo);

            client.Credentials = credential;
            client.Timeout = (60 * 5 * 1000);

            msg.From = fromMailAddress;
            msg.Subject = Subject;
            msg.IsBodyHtml = false;
            msg.To.Add(correoVis);
            msg.Body = status;
           // msg.Attachments.Add(attachment);
            client.Send(msg);



        }
    }
}
